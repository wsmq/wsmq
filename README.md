# WSMQ Playlist Generator

This app has been created to make WSMQ Playlists for the facebook group [Sounds like a weirdly specific playlist but ok
](https://www.facebook.com/groups/166072874110906/) and is avalible to use at [https://wsmq.audiofox.org](https://wsmq.audiofox.org)

## Packages Used

* [jwilsson/spotify-web-api-php](https://github.com/jwilsson/spotify-web-api-php)
* [symfony/dotenv](https://github.com/symfony/dotenv)

## License
MIT license. Please see [LICENSE.md](LICENSE.md) for more info.
