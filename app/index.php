<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta http-equiv="x-ua-compatible" content="ie=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/bootstrap.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link href="https://fonts.googleapis.com/css?family=Titan+One" rel="stylesheet">
        <title>wsmq.me</title>
    </head>

    <body id="home">
        <header>
            <!-- Navigation  -->
            <nav class="navbar navbar-dark bg-dark fixed-top">
                <div class="container">
                    <a href="index.html" class="navbar-brand">wsmq.me</a>

                    <ul class="navbar-nav ml-auto">
                        <li class="nav-itm"><a href="https://www.facebook.com/groups/166072874110906/" target="_blank" class="nav-link"><i class="fa fa-facebook"></i></a></li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- Playlister -->
        <section id="playlister" class="left nav-clearfix">
            <div class="container py-4">
                <div class="row">
                    <div class="col-md-7 d-none d-md-block">
                        <img src="assets/img/playlist.png" alt="random album" class="col-sm-12 p-0">
                    </div>

                    <div class="col-md-5">
                        <h1 class="display-5">Spotify Playlister</h1>
                        <p>Create Spotify playlists based on your listening habits.</p>
                        <a href="auth.php" class="btn btn-outline-success"><i class="fa fa-spotify"></i> Connect Spotify</a>
                    </div>
                </div>
            </div>
        </section>

        <!-- Top Ten -->
        <!-- <section id="topten" class="right">
            <div class="container py-4">
                <div class="row">
                    <div class="col-md-5">
                        <h1 class="display-5">List Creator</h1>
                        <p>Create and share your lists of your top tracks, albums, artist, and genres now.</p>
                        <a href="#" class="btn btn-outline-light"><i class="fa fa-list"></i> Make a list</a>
                    </div>

                    <div class="col-md-7 d-none d-md-block">
                        <img src="assets/img/playlist.png" alt="random album" class="col-sm-12 p-0">
                    </div>
                </div>
            </div>
        </section> -->

        <!-- Questionaire -->
        <!-- <section id="questionaire" class="left">
            <div class="container py-4">
                <div class="row">
                    <div class="col-md-7 d-none d-md-block">
                        <img src="assets/img/playlist.png" alt="random album" class="col-sm-12 p-0">
                    </div>

                    <div class="col-md-5">
                        <h1 class="display-5">Questionaire</h1>
                        <p>Ask questions and share the results with your friends</p>
                        <a href="#" class="btn btn-outline-light"><i class="fa fa-question"></i> Create a questionaire</a>
                    </div>
                </div>
            </div>
        </section> -->

        <footer>
            <!-- Information -->
            <div class="footer-copyright py-3 text-center">
                Released under the MIT Licence, find it on
                <a href="https://gitlab.com/wsmq/wsmq" target="_blank">GitLab</a>
            </div>
        </footer>

        <!-- Required Javascript -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
    </body>
</html>
