<?php

require('../vendor/autoload.php');

$api = new SpotifyWebAPI\SpotifyWebAPI();
$api->setAccessToken($_GET['t']);
$user = $api->me();

$range = 'short_term';
$playlistName = 'WSMQ Month ~ ' . $date = date("M jS Y");

switch($_GET['p']) {
    case 's':
        $range = 'short_term';
        $playlistName = 'WSMQ Month ~ ' . $date = date("M jS Y");
        break;
    case 'm':
        $range = 'medium_term';
        $playlistName = 'WSMQ Six Months ~ ' . $date = date("M jS Y");
        break;
    case 'l':
        $range = 'long_term';
        $playlistName = 'WSMQ All Time ~ ' . $date = date("M jS Y");
        break;
}

$topTracks = $api->getMyTop('tracks', "limit=50&time_range=$range");
$playlist = $api->createUserPlaylist($user->id,
    ['name'=> $playlistName,
    'description'=>
        'Created using your top songs from the last four weeks for the \'sounds like a weirdly specific music question but ok\' Facebook Group (https://www.facebook.com/groups/166072874110906/) using the AudioFox WSMQ Playlist Creator (https://wsmq.me/)',
    'public'=>true]
);
$tracks = [];

foreach ($topTracks->items as $track) {
    array_push($tracks, $track->id);
}

$api->addUserPlaylistTracks($user->id, $playlist->id, $tracks);

header('Location: app.php?t=' . $_GET['t']);
die();
