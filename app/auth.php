<?php

require('../vendor/autoload.php');

use Symfony\Component\Dotenv\Dotenv;

$dotenv = new Dotenv();
$dotenv->load(__DIR__.'/../.env');

$session = new SpotifyWebAPI\Session(
    getenv('SPOTIFY_CLIENT_ID'),
    getenv('SPOTIFY_CLIENT_SECRET'),
    getenv('SPOTIFY_REDIRECT_URI')
);

$options = [
    'scope' => [
        'user-read-private',
        'user-top-read',
        'playlist-modify-public'
    ],
];

header('Location: ' . $session->getAuthorizeUrl($options));
die();
